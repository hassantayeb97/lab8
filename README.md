Hassan Asim Tayeb 1936820

Q2 answer:
With one line of code and one method, all the observers (Email, whatsapp, facebook, twitter) will perform notify() action without mentioning every observer specificly, while every observer has a different special mechanism to perform the action. More observers can be added easily with only adding a class that implements Observer abstract class, and no other changes needed in the code.
