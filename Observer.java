package edu.kau.fcit.cpit252.observers;

public abstract class Observer {
    protected String recipient;

    public Observer(String recipient){
        this.recipient = recipient;
    }

    public abstract void notify(String message);
}
