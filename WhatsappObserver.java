package edu.kau.fcit.cpit252.observers;

public class WhatsappObserver extends Observer{
    public WhatsappObserver(String receipient){
        super(receipient);
    }

    @Override
    public void notify(String message){
        //TODO: send out a whatsapp message
        System.out.println("Send a whatsapp message");
    }
}
