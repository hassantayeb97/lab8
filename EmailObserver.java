package edu.kau.fcit.cpit252.observers;

import edu.kau.fcit.cpit252.utils.SendEmail;

public class EmailObserver extends Observer{
    public EmailObserver(String receipient){
        super(receipient);
    }

    @Override
    public void notify(String message){
        //TODO: send out an email
        SendEmail.send("Price has dropped!", message, super.recipient);
    }
}
