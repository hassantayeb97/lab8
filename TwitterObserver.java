package edu.kau.fcit.cpit252.observers;

import edu.kau.fcit.cpit252.utils.SendEmail;
import edu.kau.fcit.cpit252.utils.SendTweet;

public class TwitterObserver extends Observer{
    public TwitterObserver(String receipient){
        super(receipient);
    }

    @Override
    public void notify(String message){
        //TODO: send out a tweet
        System.out.println("Price has dropped");
    }
}
