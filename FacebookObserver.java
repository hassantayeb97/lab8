package edu.kau.fcit.cpit252.observers;

import edu.kau.fcit.cpit252.utils.SendEmail;

public class FacebookObserver extends Observer{
    public FacebookObserver(String receipient){
        super(receipient);
    }

    @Override
    public void notify(String message){
        //TODO: send out a facebook message
        System.out.println("Send a facebook message");
    }
}
